﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Microsoft.Data.SqlClient;
using WhatsAppDAL.Model;

namespace WhatsAppDAL
{
    public class WhatsAppService : IWhatsAppService
    {
        private readonly WhatsAppDbContext _dbContext;
        private bool _disposed;

        public WhatsAppService( WhatsAppDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public async Task<int> CreateUser(UserViewModel user)
        {
          var sqlConn = await _dbContext.OpenConnection();

            string insertQuery =
                $"INSERT INTO USERS (Name, Email, PhoneNumber, ProfileImage)" +
                $" VALUES ('{user.Name}', '{user.Email}', '{user.PhoneNumber}', '{user.ProfilePhoto}')";

            SqlCommand command = new SqlCommand(insertQuery, sqlConn);

            await command.ExecuteNonQueryAsync();

            string usedIdQuery =
                $"SELECT SCOPE_IDENTITY();";

            command.CommandText = usedIdQuery;

            SqlDataReader reader = command.ExecuteReader(CommandBehavior.CloseConnection);

            while (reader.Read())
            {
              return  (int) (decimal) reader.GetValue(0);
            }

            return 0;



        }

        public void UpdateUser(UserViewModel user)
        {
            throw new System.NotImplementedException();
        }

        public void DeleteUser(int id)
        {
            throw new System.NotImplementedException();
        }

        public UserViewModel GetUser(int id)
        {
            throw new System.NotImplementedException();
        }

        public IEnumerable<UserViewModel> GetUsers()
        {
            throw new System.NotImplementedException();
        }


        protected virtual void Dispose(bool disposing)
        {

            if (_disposed)
            {
                return;
            }
             
            if (disposing)
            {
                _dbContext.Dispose();
            }

            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}